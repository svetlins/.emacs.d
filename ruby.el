(add-hook 'ruby-mode-hook
	  (lambda ()
	    (ruby-end-mode)
	    (ruby-tools-mode)
            (my-turn-on-robe-and-company)))

(dolist (regex '("\\.rake$" "\\.gemspec$" "\\.ru$" "Rakefile$" "Gemfile$" "Capfile$" "Guardfile$"))
  (add-to-list 'auto-mode-alist `(,regex . ruby-mode)))

(defun hlt-ruby-method ()
  "Highlight the ruby method that is currently under point."
  (interactive)
  (mark-defun)
  (hlt-highlight))

(defun my-turn-on-robe-and-company ()
  ; (robe-mode)
  (company-mode))
  ;(push 'company-robe company-backends))
