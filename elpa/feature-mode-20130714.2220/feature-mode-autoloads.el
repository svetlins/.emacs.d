;;; feature-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (feature-mode) "feature-mode" "feature-mode.el"
;;;;;;  (21126 7177 0 0))
;;; Generated autoloads from feature-mode.el

(autoload 'feature-mode "feature-mode" "\
Major mode for editing plain text stories

\(fn)" t nil)

(add-to-list 'auto-mode-alist '("\\.feature\\'" . feature-mode))

;;;***

;;;### (autoloads nil nil ("feature-mode-pkg.el") (21126 7178 157056
;;;;;;  0))

;;;***

(provide 'feature-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; feature-mode-autoloads.el ends here
